// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
app = angular.module('starter', ['ionic'])


app.controller( 'LibertyController', function($scope, $ionicModal, $http, $ionicSideMenuDelegate, $ionicGesture) {
  // No need for testing data anymore
  $scope.show = false;
  $scope.tasks = [];
  var names = $http.get("js/miserables.json").success(function(data) {
  var nodes = data.nodes;
    angular.forEach(nodes, function(data) {
      $scope.tasks.push({title: data.name});
    });
  });
  // Create and load the Modal
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
  //$ionicSideMenuDelegate.shouldDrag = true;
  //$scope.enabled=false;
  $scope.toogleRightClick = function() {
    $ionicSideMenuDelegate.edgeDragThreshold(75);
    $ionicSideMenuDelegate.toggleRight();
  };
})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)c
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});