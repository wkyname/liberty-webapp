// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
app = angular.module('starter', ['ionic'])


  .config(function($httpProvider){
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  })
  .service('sharedProperties', function($rootScope) {
      var sharedProperties = this;
      sharedProperties.json = "Default";

  });

// app.server = "http://localhost:8080/";
app.server = "http://52.11.184.148/";
app.json = null;
app.force = null;

app.directive("myWidget", function() {
  return {
    restrict: "E",
    replace: true,
    templateUrl: "GraphScreen.html"
  };
});
app.directive("preferences", function() {
  return {
    restrict: "E",
    replace: true,
    templateUrl: "preferences.html"
  }
});

app.controller('PreferenceController', function($scope, $http, $filter, $window, sharedProperties) {
  $scope.nodes = [];
  var query = app.server+"vertices/type";
  var response = $http.get(query).success(function(data) {
    data.forEach(function(d) {
      $scope.nodes.push({node: d, isSelected: ""});
    })
  });
  $scope.nodeClicked = function(event) {
    //console.log(event);
    var target = event.target.id;
    //$scope.nodes[$scope.nodes.indexOf(target)].isSelected = true;
    var filtered = $filter('filter')($scope.nodes, {node: target})[0];
    if (filtered.status === "selected")
      filtered.status = "";
    else
      filtered.status = "selected";


    // var query = app.server+"vertices/type/"+type;
    // var response = $http.get(query).success(function(data) {
    //   $scope.nodes = [];
    //   data.nodes.forEach(function(n) {
    //     $scope.nodes.push(n.name);
    //   })
    // })
  };
  $scope.sendQuery = function(event) {
    var queryArray = [];
    $scope.nodes.forEach(function(n) {
      if (n.status === "selected")
        queryArray.push(n.node);
    });
    if (queryArray.length == 0)
      alert("Not Cool");
    else {
      console.log(queryArray);
      console.log(JSON.stringify(queryArray));
      var query = app.server + "select/"+ JSON.stringify(queryArray);
      $http.get(query).success(function(data) {
        //console.log(data);
        console.log(sharedProperties.json);
        $window.localStorage["sharedData"]= JSON.stringify(data);
        console.log(window.localStorage.length);
        //sharedProperties.json = data;
        //console.log(sharedProperties.json);
        //$window.location.href = "/GraphScreen.html";  
      }).then(function() {
         $window.location.replace("GraphScreen.html"); 
      });
    }
  }
  $scope.nodeSelected = function(event) {
    console.log(event);
  }
});

app.controller('MapController', function($scope, $http) {

  var nodes;
  var map;

  // initialize map centered around Bristol
  function initialize() {
    console.log("jes"); 
    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(51.4500,-2.5833) // Bristol location
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);  
    // var database = $http.get("js/miserables.json").success(function(data) {

      nodes = app.json.nodes;      
      console.log("CUAE", nodes);
      angular.forEach(nodes, function(data) { 

        var location = []; 
        
        if(data.location !== undefined && data.location !== "")
        { 
          // format of location: "lat : long"
          location = data.location.split(" "); 
        }
        else if(data.currentLocation !== undefined && data.currentLocation !== "")
        {
          // format of currentLocation: "lat : long"
          location = data.currentLocation.split(" "); 
        }

        var lat = location[0]; // get latitude
        var lng = location[2]; // get longitude 

        // create marker on map with previously fetched coordinates
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(parseFloat(lat),parseFloat(lng)),
          map: map,
          title: data.name
        }); 
        google.maps.event.removeListener(initListener);            
        

      });          
    // }); 
  }
  
  var mapDiv = document.getElementById('toggleMap');
  var initListener = google.maps.event.addDomListener(mapDiv, 'click', initialize); 
})


app.controller('LibertyController', function($scope, $filter, $ionicModal, $http, $ionicSideMenuDelegate, $ionicGesture, $window) {
//app.controller('LibertyController', function($scope, $ionicModal, $http, $ionicSideMenuDelegate, $ionicGesture) {
  // No need for testing data anymore
  $scope.show = false;
  $scope.tasks = [];
  $scope.types = [];
  $scope.labels = [];
  app.json = JSON.parse($window.localStorage["sharedData"]);
  //console.log(myObj);
  var nodes;
  var edges;
  var t = []; var e = [];
  // /console.log(sharedProperties.json);
  //var names = $http.get(app.json).success(function(data) {
  // var names = $http.get("js/miserables.json").success(function(data) {
    nodes = app.json.nodes;
    edges = app.json.links;
    angular.forEach(nodes, function(data) {
        $scope.tasks.push({title: data.name});


        //get the types of the nodes (make sure they are unique)
      if(t.indexOf(data.type)<0)
      {
        t.push(data.type);
        $scope.types.push({title: t[t.length-1], isSelected: true});
        //t.sort();
      }  
    //});
    angular.forEach(edges, function(data){
      //$scope.labels.push({title: data._label});

      if(e.indexOf(data._label)<0)
      {
        e.push(data._label);
        $scope.labels.push({title: e[e.length-1], isSelected: true});
      }
    });
    console.log($scope.tasks);
  });
  // Create and load the Modal
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
  $ionicModal.fromTemplateUrl('new-node.html', function(modal) {
    $scope.addNewModal = modal;

  }, {
    scope: $scope,
    animation: 'slide-in-up'
  })
  //$ionicSideMenuDelegate.shouldDrag = true;
  //$scope.enabled=false;
  $scope.toogleRightClick = function() {
    $ionicSideMenuDelegate.edgeDragThreshold(75);
    $ionicSideMenuDelegate.toggleRight();
  };

  $scope.toogleLeftClick = function() {
    $ionicSideMenuDelegate.edgeDragThreshold(75);
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.nodeClicked = function(event, json) {
   // console.log(event.target.id);
      //console.log(nodes);
      $scope.tasks.splice($scope.tasks.indexOf(event.target.id),1);
    }

  $scope.toggleMap = function() {
    var x =document.getElementById("map-canvas").style
    console.log(x);
    if (x.display == "none"){
      console.log("not displaying");
      x.display= "block";
    }
    else {
      console.log("displaying");
      x.display = "none";
    }
  }


  $scope.getNodeTypes = function(json) {

    var nodes = [];           // list of all nodes contained in the json file (database)
    var nodeTypes = [];       // list with (unique) types of nodes
    nodes = json.nodes;

    for(var n in nodes) {
      var currentType = n["type"];

      if(!nodeTypes.includes(currentType)){
        nodeTypes.push(currentType);
      }
    }

  }

  $scope.showNodes = function() {
    //$scope.taskModal.show();
    //$scope.taskModal.hide();
    console.log(this.show);
    this.show = !this.show;

  }
  $scope.moveGraph = function(node) {

  }
  // Called when the form is submitted
  $scope.createTask = function(task) {
    $scope.tasks.push({
      title: task.title
    });
    $scope.taskModal.hide();
    task.title = "";
  };

  // Open our new task modal
  $scope.newTask = function() {
    $scope.taskModal.show();
  };

  $scope.createNode = function() {
    var types = [];

    $scope.types.forEach(function(t) {
      types.push(t.title);
    });
    var query = app.server + "select/notin/"+ JSON.stringify(types);
    $http.get(query).success(function(data) {
      $scope.querySuccess =[];
      data.forEach(function(d) {
        $scope.querySuccess.push({node: d, isSelected: ""});
      })
    }).then(function() {
    $scope.addNewModal.show();});
  }
  $scope.nodeClicked = function(event) {
    //console.log(event);
    var target = event.target.id;
    //$scope.nodes[$scope.nodes.indexOf(target)].isSelected = true;
    console.log($scope.querySuccess);
    var filtered = $filter('filter')($scope.querySuccess, {node: target})[0];
    if (filtered.status === "selected")
      filtered.status = "";
    else
      filtered.status = "selected";


    // var query = app.server+"vertices/type/"+type;
    // var response = $http.get(query).success(function(data) {
    //   $scope.nodes = [];
    //   data.nodes.forEach(function(n) {
    //     $scope.nodes.push(n.name);
    //   })
    // })
  };
  $scope.sendQuery = function(event) {
    //console.log(app.force);
    console.log($scope.querySuccess);
    var queryArray = []
    $scope.querySuccess.forEach(function(n) {
      if (n.status === "selected")
        queryArray.push(n.node);
    });
    if (queryArray.length == 0)
      alert("Not Cool");
    else {
      console.log(queryArray);
      console.log(JSON.stringify(queryArray));
      var query = app.server + "select/"+ JSON.stringify(queryArray);
      console.log(query);
      $http.get(query).success(function(data) {
        //console.log(data);
        //console.log(sharedProperties.json);
        //$window.localStorage["sharedData"]= JSON.stringify(data);
        console.log(data);
        addNode(data.nodes, data.links, app.force);
        console.log(window.localStorage.length);
        //sharedProperties.json = data;
        //console.log(sharedProperties.json);
        //$window.location.href = "/GraphScreen.html";  
      }).then(function() {
         //addNode()
         $scope.addNewModal.hide();
      });
    }
  }
  // Close the new task modal
  $scope.closeNewTask = function() {
    $scope.taskModal.hide();
  };
  $scope.selectNodes = function() {
    console.log($scope.types);
  }
  $scope.closeNewNode = function() {
    $scope.addNewModal.hide();
    console.log(app.force);
  }

})


.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)c
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    if (app.json != null) 
      app.force = mainBody(app.server, app.json);
      console.log(app.force);
  });
});

  var color = d3.scale.category20();

  //Move Graph button functionality 
  // d3.selectAll("#moveGraph").on("tap", function(d){
  //     //console.log(pushed);
  //     //console.log(d3.event.translate);
  //     if(pushed){
  //       pushed = !pushed;
  //       node_drag = d3.behavior.drag()
  //       .on("dragstart", null)
  //       .on("drag", null)
  //       .on("dragend", null); 
  //       d3.selectAll("g.node").call(node_drag);
  //       d3.selectAll("#moveGraph").style("color","black");
  //       // zoom = d3.behavior.zoom()
  //       //   .on("zoom" , null); 
  //       // d3.selectAll(".mainBody")
  //       //   .call(zoom);        
  //     }
  //     else {
  //       pushed = !pushed;
  //       node_drag = d3.behavior.drag()
  //       .on("dragstart", dragstart)
  //       .on("drag", dragmove)
  //       .on("dragend", dragend);
  //       d3.selectAll("#moveGraph").style("color","blue"); 
  //       d3.selectAll("g.node").call(node_drag);
  //       // zoom = d3.behavior.zoom()
  //       //                 .scaleExtent([1,10])
  //       //                 .on("zoom",zoomedOn)
                        
  //       // d3.selectAll(".mainBody").call(zoom);
  //     }
  //   });


  function tickMe(force) {
    d3.selectAll("line.link").attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    d3.selectAll("g.node").attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  }


  //doesnt work properly yet
  function addNode(newNodes, newEdgesSet, force) {
      var newEdges = [];
      var nodes = force.nodes();  
      newNodes.forEach(function(n) {
        nodes.push(n);
      });
      force.nodes(nodes);
    // var stringie = JSON.stringify(newNodes);
    // console.log(stringie);
    // var obj = JSON.parse(stringie);
    // var list = [];
    // obj.forEach(function(o) {
    //   list.push(o._id);
    // })
    // console.log(list);
    // list = JSON.stringify(list);
    // list = list.replace("[","[(long)").split(",").join(",(long)");
    // console.log(list);

    //console.log("noduri", force.nodes());
    newEdgesSet.forEach(function(e) {
      var sourceNode = nodes.filter(function(n) { return n._id === e._inV; })[0],
          targetNode = nodes.filter(function(n) { return n._id === e._outV;})[0];
      if (sourceNode != undefined && targetNode != undefined) {
        newEdges.push({source: sourceNode, target: targetNode, id:e._id});
        force.links().push({source: sourceNode, target: targetNode, id:e._id});
      }
    });
    var edgeEnter = d3.select("svg").select("g")
    edgeEnter = edgeEnter.selectAll("line.link").data(newEdges, function(d) { return d.id; }).enter().append("svg:line")
    //console.log(edgeEnter);
          .attr("class", "link")
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
      var nodeEnter = d3.select("svg").select("g");
      nodeEnter = nodeEnter.selectAll("g.node").data(newNodes, function(d){ return d._id;}).enter()
      //console.log("mda", nodeEnter);
      nodeEnter = nodeEnter.append("svg:g")
        .attr("class", "node")
        .call(force.drag);
        //console.log(nodeEnter);
      //draw circles
        nodeEnter.append("svg:circle")
        .attr("class", "circle")
        .attr("r",15)
        .attr("id", function(d) { return "n"+d._id;})
        .style("fill", function(d) {return color(d.type);})
        .attr("x", "-8px")
        .attr("y", "-8px")
        .attr("width", "16px")
        .attr("height", "16px")
        .on("tap", function(d){ highlight(d, force);})
        .on("hold", function(d){ removeNode(d, force);}); 
      //draw text
      nodeEnter.append("svg:text")
          .attr("class", "nodetext")
          .attr("dx", 12)
          .attr("dy", ".35em")
          .text(function(d) { return d.name });    
      
      //update list of nodes

    //assign new set of Edges 
    force.start();
   // start(force);
         
    

     
  }

  function start(force){
    var link = null;

    force.stop();
    force.start();

    link = d3.selectAll("line.link")
            .data(force.links())
            .exit().remove()
            .append("svg:line")
            .attr("class", "link")
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

    d3.selectAll("g.node").remove();

    var node = d3.select("g").selectAll("g.node")
                 .data(force.nodes())
                 .enter().append("svg:g")
                 .attr("class", "node");

    node.append("svg:circle")
        .attr("class", "circle")
        .attr("r",15)
        .attr("id", function(d) { return "n"+d._id;})
        .style("fill", function(d) {return color(d.type);})
        .attr("x", "-8px")
        .attr("y", "-8px")
        .attr("width", "16px")
        .attr("height", "16px")
        .on("tap", function(d){ highlight(d, force);})
        .on("hold", function(d){ removeNode(d, force);});

    node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 12)
        .attr("dy", ".35em")
        .text(function(d) { return d.name });

    d3.selectAll("text").data(force.nodes())
      .attr("class", "nodetext")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d) { return d.name });
  }

   
  function removeNode(what, force) {
    // var nodes = force.nodes();
    var newEdges = [];
    
    //remove the node "what" from the nodes list
    force.nodes().splice(force.nodes().indexOf(what),1);

    //check the updated nodes list and only add the edges which are relevant
    force.links().forEach(function(e) {
      if (!(e.source === what || e.target === what)) {
        newEdges.push(e);
      }
    });

    //assign new set of Edges 
    force.links(newEdges)
         .start();
    start(force);
  }
        
          

  function highlight(what,force) {
    //repaint the color of all nodes by their type
    d3.selectAll("g.node")
      .select("circle")
      .style("fill", function(d){ return color(d.type);});

    //change the color of the selected node
    d3.selectAll("#n"+what._id).style("fill", "rgb(0,0,0)");
    
    force.start();

    //stick the info about the selected node on top of the screen 
    appendInfo(what);

  }

  function appendInfo(node){
    d3.selectAll("#nodeInformation li").remove();
    var infoDiv = document.getElementById("nodeInformation");
    var listElem = document.createElement("li");

    for(var property in node){
      if(!(property == "_id" ||  
           property == "_type" ||
           property == "location"||
           property == "currentLocation" ||
           property == "index"||
           property == "time" ||
           property == "weight" ||
           property == "x" || 
           property == "y" || 
           property == "px" ||
           property == "py"
           )){
      var listElem = document.createElement("li");
      listElem.appendChild(document.createTextNode(property +" : "+ node[property]));
      infoDiv.appendChild(listElem); 
      }
    }
  }



var mainBody = function(path, json, appForce) {
  var margin = {top: -5, right: -5, bottom: -5, left: -5};
  var width = parseInt(d3.select(".view").style('width')),
      height = 450;

  var force = d3.layout.force()
      .charge(-200)
      .linkDistance(150)
      .size([width, height]);


  var zoom = d3.behavior.zoom()
               .scaleExtent([1,10])
               .on("zoom",null);

  var node_drag = d3.behavior.drag()
                    .on("dragstart", dragstart)
                    .on("drag", dragmove)
                    .on("dragend", dragend);
        

  function dragstart(d, i) {
    force.stop() // stops the force auto positioning before you start dragging
    d3.event.sourceEvent.stopPropagation();
  }


  function dragmove(d, i) {
      dragaction(d,i);
  }

  function dragaction(d,i) {
      d.px += d3.event.dx;
      d.py += d3.event.dy;
      d.x += d3.event.dx;
      d.y += d3.event.dy; 
      tickMe(force); // this is the key to make it work together with updating both px,py,x,y on d !
  }

  function dragend(d, i) {
      d.fixed = true; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
      tickMe(force);
      //d3.event.sourceEvent.startPropagation();
      force.resume();
  }


  //append the SVG to the mainBody 
  var vis = d3.select(".mainBody").append("svg:svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
      .call(zoom);

  var attributes= [];
  var scale1 = 1;
  var translate = [0,0];

  function zoomedOn() {
    vis.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");

    attributes = vis.attr("transform").replace("translate(","")
                                      .split(")").join("")
                                      .replace("(","")
                                      .split("scale");

    scale1 = Number(attributes[1]);
    translate = attributes[0].split(",");
    translate[0] = Number(translate[0]);
    translate[1] = Number(translate[1]);
  };

  function zoomedOff() {
    vis.attr();
  };
  //read database/json file for nodes to start with 
  //d3.json(path, function(json) {

    var pushed = false; 
    var edges = [];
    var nodes = [];

    var newNode = [{
      _id: 1,
      _type: "vertex",
      location: "36.56 : 18.45",
      name: "constanta",
      px: 585.2035766790905,
      py: 254.76828409474902,
      type: "port",
      weight: 3,
      x: 274,
      y: 254.77978025070527
    }, {     _id: 2,
      _type: "vertex",
      location: "36.56 : 18.45",
      name: "maria",
      px: 585.2035766790905,
      py: 254.76828409474902,
      type: "port",
      weight: 3,
      x: 585.1724713616062,
      y: 254.77978025070527
    }]

    
    

    nodes = json.nodes;
    fullListNodes = JSON.parse(JSON.stringify(nodes));
    

    //create the set of edges
    //console.log(json.links);
    json.links.forEach(function(e) {
      var sourceNode = nodes.filter(function(n) { return n._id === e._inV; })[0],
          targetNode = nodes.filter(function(n) { return n._id === e._outV;})[0];
      if (sourceNode !== undefined && targetNode !== undefined)
      edges.push({source: sourceNode, target: targetNode, id: e._id});
    });



    var force = self.force = d3.layout.force()
        .nodes(nodes)
        .links(edges)
        .gravity(.05)
        .distance(100)
        .charge(-100)
        .size([width, height])
        .start();

    //draw the set of edges
    function drawEdges(edgeList){
      vis.selectAll("line.link")
          .data(edgeList)
          .enter().append("svg:line")
          .attr("class", "link")
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
    }
    drawEdges(edges);
    


    //initialize drag behaviour to null  ( no dragging allowed)
    var node_drag = d3.behavior.drag()
        .on("dragstart", null)
        .on("drag", null)
        .on("dragend", null);

    

    function dragstart() {
      force.stop() // stops the force auto positioning before you start dragging
      //d3.event.sourceEvent.stopPropagation();
      d3.event.sourceEvent.stopPropagation();
    }

    function dragmove(d, i) {
      dragaction(d,i);
    }

    function dragaction(d,i) {
        d.px += d3.event.dx;
        d.py += d3.event.dy;
        d.x += d3.event.dx;
        d.y += d3.event.dy; 

        tickMe(force); // this is the key to make it work together with updating both px,py,x,y on d !
    }

    function dragend(d, i) {
      d.fixed = true; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
      tickMe(force);
      //d3.event.sourceEvent.startPropagation();
      force.resume();
    }

    //draw the nodes
    function drawNodes(nodeList){
      force.nodes(nodeList).start();

      var nodeDraw = vis.selectAll("g.node")
          .data(nodeList)
          .enter().append("svg:g")
          .attr("class", "node")
          .call(node_drag);

      nodeDraw.append("svg:circle")
          .attr("class", "circle")
          .attr("r",15)
          .attr("id", function(d) { return "n"+d._id;})
          .style("fill", function(d) {return color(d.type);})
          .attr("x", "-8px")
          .attr("y", "-8px")
          .attr("width", "16px")
          .attr("height", "16px")
          .on("tap", function(d){ 
            highlight(d, force);})
          .on("hold", function(d){ removeNode(d, force);});

      nodeDraw.append("svg:text")
          .attr("class", "nodetext")
          .attr("dx", 12)
          .attr("dy", ".35em")
          .text(function(d) { return d.name });
    }

    //nodes.push(newNode[0]);
    //nodes.push(newNode[1]);

    drawNodes(nodes);

    force.on("tick", function(d) { return tickMe(force);});
  

   //Move Graph button functionality 
   d3.selectAll("#moveGraph").on("tap", function(d){
      if(pushed){
        pushed = !pushed;
        node_drag = d3.behavior.drag()
        .on("dragstart", null)
        .on("drag", null)
        .on("dragend", null); 
        d3.selectAll("g.node").call(node_drag);
        d3.selectAll("#moveGraph").style("color","black");
        zoom = d3.behavior.zoom()
          .on("zoom" , null); 
        zoomBehave = d3.selectAll("svg");
        zoomBehave.on("mousemove.zoom",null);
        zoomBehave.on("dblclick.zoom",null);
        zoomBehave.on("touchstart.zoom",null);
        zoomBehave.on("wheel.zoom",null);
        zoomBehave.on("mousewheel.zoom",null);
        zoomBehave.on("MozMousePixelScroll",null);
          zoomBehave.call(zoom);        
      }
      else {
        pushed = !pushed;
        node_drag = d3.behavior.drag()
        .on("dragstart", dragstart)
        .on("drag", dragmove)
        .on("dragend", dragend);
        d3.selectAll("#moveGraph").style("color","blue"); 
        d3.selectAll("g.node").call(node_drag);
        zoom = d3.behavior.zoom()
                        .scaleExtent([1,10])
                        .scale(scale1)
                        .translate([translate[0], translate[1]])
                        .on("zoom",zoomedOn);
                        
        d3.selectAll("svg").call(zoom);
      }
    }); 
      
  var types = [];                  
  d3.selectAll(".type-button")[0].forEach(function(type) {
    if (type.id != "")
      types.push(type.id);
  })
  console.log(d3.selectAll(".type-button"));
  //types = ["port","voyage","vesselHMSsaves","event","country","boat"]
  //remove groups of nodes list functionality
  d3.selectAll(".type-button").data(types).on("tap", function(event) {
    //console.log(event);
    var toRemove = [];
    var toAddNodes =[];
    var toAddEdges;
    console.log(event);
    for(i in force.nodes()){
        // console.log(force.nodes()[i].type);
      if(force.nodes()[i].type == event) {
        toRemove.push(force.nodes()[i]);
      }
    }
    if(toRemove.length==0) {
      console.log("E GOALAALAAAAA");
      //console.log(fullListNodes);

        var reqPath= path+'vertices/type'+'/'+event;
    //     var xhr = d3.json(reqPath)
    // .on("progress", function() { console.log("progress", d3.event.loaded); })
    // .on("load", function(json) { console.log("success!", json); toAddNodes = json.nodes; return toAddNodes;})
    // .on("error", function(error) { console.log("failure!", error); })
    // console.log(xhr.load);
    console.log(reqPath);
        d3.json(reqPath).on("load", function(data){
          var myLinks = data.links;
          var myNodes = data.nodes;
          console.log("links", myLinks,"nodes", myNodes);
          addNode(myNodes, myLinks, force);
        }).get();
        
      
  
      
    }
    for(i in toRemove) {
      removeNode(toRemove[i], force);
    }
    types.splice(types.indexOf(event), 1);
  });

  d3.selectAll("#addNewNodes").on("tap", function(event) {
    console.log("yeesss");
    var queryArray = [];
    var selectedTypes = d3.selectAll("#addNewNodes .selected")
    console.log(selectedTypes);

  });
  return force;
    //create node button functionality 
    // d3.selectAll("#createNode").on("tap", function(d){ 
    //   for(i in newNode)
    //     addNode(newNode[i], force);
    // });
    //});    
}


// THE END
