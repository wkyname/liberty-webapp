
app = angular.module('mymap', ['ionic'])


  .config(function($httpProvider){
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  });


app.controller('MapController', function($scope, $http) {

  var nodes;
  
  var database = $http.get("miserables.json").success(function(data) {

    nodes = data.nodes;
    console.log(nodes);
    
    //define map options
    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(51.4500,-2.5833) // Bristol location
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    
    
    angular.forEach(nodes, function(data) {
      if(data.location != undefined)
      { 
        // format of location: "lat : long"
        var location = data.location.split(" ");
        var lat = location[0]; // get latitude
        var lng = location[2]; // get longitude

        // create marker on map with previously fetched coordinates
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat,lng),
          map: map,
          title: data.name
        });
      }
    });

    //google.maps.event.addDomListener(window, 'load', initialize);

  });  
})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)c
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    //google.maps.event.addDomListener(window, 'load', initialize);
  });
});


/*  var myLatlng = new google.maps.LatLng(51.4500,-2.5833); // Bristol coordinates
  var mapOptions = {
    zoom: 4,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Hello World!'
  });
}
*/
//google.maps.event.addDomListener(window, 'load', initialize);
